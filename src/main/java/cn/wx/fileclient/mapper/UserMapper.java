package cn.wx.fileclient.mapper;

import cn.wx.fileclient.entity.SysUser;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * cn.wx.fileclient.mapper
 * name：UserMapper
 * user：pengpeng
 * date:2020/11/2
 * time:12:51
 */
public interface UserMapper {
    SysUser findByUser(Map<String,Object> paramMap);
    SysUser findByUsername(String username);

}
