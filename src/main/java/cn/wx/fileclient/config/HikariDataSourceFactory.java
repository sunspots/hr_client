package cn.wx.fileclient.config;

import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.datasource.unpooled.UnpooledDataSourceFactory;

/**
 * @description: 连接池配置类
 * @author: VNElinpe
 * @date: 2020/7/18 17:29
 */
public class HikariDataSourceFactory extends UnpooledDataSourceFactory {

    public HikariDataSourceFactory() {
        super.dataSource=new HikariDataSource();
    }
}
