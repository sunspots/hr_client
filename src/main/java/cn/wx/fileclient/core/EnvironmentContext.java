package cn.wx.fileclient.core;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @description:
 * @author: VNElinpe
 * @date: 2020/7/21 0:01
 */
@Slf4j
public class EnvironmentContext {
    public static String getProperties(String key){
        Properties properties=new Properties();
        try(InputStream in=Resources.getResourceAsStream("environment.properties")) {
            properties.load(in);
        } catch (IOException e) {
            log.error("load environment.properties fail! {}",e.getMessage());
        }
        return properties.getProperty(key);
    }
}
