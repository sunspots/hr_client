package cn.wx.fileclient.test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * cn.wx.fileclient.test
 * name：TreeNode
 * user：pengpeng
 * date:2020/11/3
 * time:14:10
 */
public class TreeNode {
    private Integer id;
    private Integer parentId;
    private Integer score;
    private List<TreeNode> child = new ArrayList<>();

    public List<TreeNode> getChild() {
        return child;
    }

    public void setChild(List<TreeNode> child) {
        this.child = child;
    }

    public void addChild(TreeNode child) {
       this.child.add(child);
    }

    public Integer getId() {
        return id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public Integer getScore() {
        return score;
    }

    public TreeNode() {
    }

    public TreeNode(Integer id, Integer parentId, Integer score) {
        this.id = id;
        this.parentId = parentId;
        this.score = score;
    }
}
