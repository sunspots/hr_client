package cn.wx.fileclient.service;

import javafx.scene.control.Alert;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * 文件扫描线程
 * Created by IntelliJ IDEA.
 * cn.wx.fileclient.service
 * name：FileScan
 * user：pengpeng
 * date:2020/11/2
 * time:16:55
 */
public class FileScan implements Callable<List<String>> {

    private String fileUrl;
    List<String> fileList;

    public FileScan() {
        //扫描文件默认 D盘
        this("D:/", new ArrayList<>());
    }

    public FileScan(String fileUrl) {
        this(fileUrl, new ArrayList<>());
    }

    public FileScan(String fileUrl, List<String> fileList) {
        this.fileUrl = fileUrl;
        this.fileList = fileList;
    }

    //循环遍历文件,并将文件加入文件列表中
    private void ergodicFile(File file, List<String> fileList) {
        //成功文件夹不能扫描，用于存储扫描成功文件
        if (null == file || "success_Dir".equals(file.getName()) || "fail_Dir".equals(file.getName()))
            return;
        if (file.isDirectory()) {
            File[] fs = file.listFiles();
            if (null == fs)
                return;
            for (File f : fs) {
                if (f.isDirectory())  //若是目录，则递归打印该目录下的文件
//                    ergodicFile(f, fileList);
                if (f.isFile()) {
                    //若是文件，且格式合法，加入文件列表
                    String fileType = f.getPath().substring(1 + f.getPath().lastIndexOf("."));
                    if (fileType != null && ("pdf".equals(fileType) || "doc".equals(fileType)
                            || "docx".equals(fileType) || "png".equals(fileType)
                            || "jpg".equals(fileType) || "gif".equals(fileType)
                            || "HEIC".equals(fileType))) {
                        fileList.add(file.getAbsolutePath());
                    }
                }
            }
        } else {
            fileList.add(file.getPath());
        }

    }

    private void alert(String title, String subject, String content) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(subject);
        alert.setContentText(content);
        alert.showAndWait();
    }

    @Override
    public List<String> call() throws Exception {
        if (StringUtils.isEmpty(fileUrl)) {
            System.out.println("文件夹为空");
        }
        File files = new File(fileUrl);
        if (files.isDirectory()) {
            // 如果是文件夹，则扫描加入文件列表
            ergodicCurrentFile(files, fileList);
//            ergodicFile(files, fileList);
        } else {
            fileList.add(files.getAbsolutePath());
        }
        return fileList;
    }

    private void ergodicCurrentFile(File file, List<String> fileList) {
        //成功文件夹不能扫描，用于存储扫描成功文件
        if (null == file || "success_Dir".equals(file.getName()) || "fail_Dir".equals(file.getName()))
            return;
        if (file.isDirectory()) {
            File[] fs = file.listFiles();
            if (null == fs)
                return;
            for (File f : fs) {
//                if (f.isDirectory())  //todo 若是目录，暂时不做
                if (f.isFile()) {
                    //若是文件，且格式合法，加入文件列表
                    String fileType = f.getPath().substring(1 + f.getPath().lastIndexOf("."));
                    System.out.println("文件类型：" + fileType);
                    if (fileType != null && (("pdf".equals(fileType) || "doc".equals(fileType)
                            || "docx".equals(fileType) || "png".equals(fileType)
                            || "jpg".equals(fileType) || "gif".equals(fileType)
                            || "HEIC".equals(fileType)))) {
                        fileList.add(f.getPath());
                    }
                }
            }
        } else {
            fileList.add(file.getPath());
        }
    }
}
