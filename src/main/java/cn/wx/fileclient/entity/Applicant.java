package cn.wx.fileclient.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Applicant implements Serializable {

    private Long id;
    private String name;
    private String phone;
    private String email;
    private Date createTime;

}
