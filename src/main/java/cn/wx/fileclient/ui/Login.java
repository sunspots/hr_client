package cn.wx.fileclient.ui;

import cn.wx.fileclient.config.Environment;
import cn.wx.fileclient.entity.SysUser;
import cn.wx.fileclient.factory.MybatisSqlSessionFactory;
import cn.wx.fileclient.mapper.UserMapper;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.apache.ibatis.session.SqlSession;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * cn.wx.fileclient.ui
 * name：Login
 * user：pengpeng
 * date:2020/11/2
 * time:11:55
 */
public class Login implements FileClientWindow {

    private Stage stage;
    private FileClientWindow parent;

    public Login(FileClientWindow parent, Stage stage) throws FileNotFoundException {
        this.stage = stage;
        this.parent = parent;
        start();
    }

    public void start() throws FileNotFoundException {

        this.stage.setWidth(600);
        this.stage.setHeight(550);
        this.stage.getIcons().add(new Image(Login.class.getResourceAsStream("/images/icon.png")));
//        Label title = new Label("欢迎使用商标服务系统");
        Image image = new Image(Login.class.getResourceAsStream("/images/icon.png"));

        stage.isResizable();
        ImageView imageView = new ImageView(image);
//        title.setStyle("-fx-font-size: 25");
//        title.setStyle("-fx-text-alignment: center");
        Label name = new Label("用户名：");
        Label password = new Label("密  码：");
//            Label pin = new Label("U盾PIN码：");
        final TextField usernameInput = new TextField();
        usernameInput.setPrefWidth(150);
        final PasswordField passwordInput = new PasswordField();
        final PasswordField pinInput = new PasswordField();
        Button clear = new Button("清除");
        Button login = new Button("登录");
        Text pop = new Text("");
        pop.setStyle("-fx-font-style: red");
        pop.setStyle("-fx-font-style: red");
        //网格布局
        GridPane gridPane = new GridPane();
        gridPane.setHgap(20);
        gridPane.setVgap(20);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
//        gridPane.add(title,0,0,2,1);
        gridPane.add(imageView, 0, 0, 2, 1);

        gridPane.add(name, 0, 1);
        gridPane.add(usernameInput, 1, 1);

        gridPane.add(password, 0, 2);
        gridPane.add(passwordInput, 1, 2);

//            gridPane.add(pin, 0, 3);
//            gridPane.add(pinInput, 1, 3);

        gridPane.add(clear, 0, 4);
        gridPane.add(login, 1, 4);
        gridPane.add(pop, 1, 5);

        //设置单独组件的上下左右的间距
        GridPane.setMargin(login, new Insets(0, 0, 0, 120));
        GridPane.setMargin(pop, new Insets(0, 0, 0, 120));
        gridPane.setAlignment(Pos.CENTER);
        //设置垂直间距
        gridPane.setVgap(10);
        //设置水平间距
        gridPane.setHgap(5);

        Scene scene = new Scene(gridPane);
        scene.getStylesheets().add(
                getClass().getResource("/ui/LoginStyle.css")
                        .toExternalForm());
        stage.setScene(scene);
        stage.setTitle("登录");

        //绑定监听事件 clear清除
        clear.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //单击清除用户名和密码
                usernameInput.clear();
                passwordInput.clear();
                pinInput.clear();
            }
        });
        FileClientWindow self = this;
        //对登录按钮绑定监听事件
        login.setOnAction(new EventHandler<ActionEvent>() {
            SqlSession sqlSession = MybatisSqlSessionFactory.getSqlSession();
            UserMapper mapper = sqlSession.getMapper(UserMapper.class);
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

            public void handle(ActionEvent event) {
                if (usernameInput.getText().isEmpty()) {
                    pop.setText("请输入用户名");
                    return;
                }

                if (passwordInput.getText().isEmpty()) {
                    pop.setText("请输入密码");
                    return;
                }
//                if (pinInput.getText().isEmpty()) {
//                    pop.setText("请输入商标局U盾的pin码");
//                    return;
//                }
                //判断用户名密码
                SysUser sysUser = mapper.findByUsername(usernameInput.getText().trim().toString());
                String password = sysUser.getPassword();
                if (sysUser == null) {
                    pop.setText("请输入正确的用户名");
                    return;
                }
                String inputStrPass = passwordInput.getText().trim();
                if (!passwordEncoder.matches(inputStrPass, password)) {
                    pop.setText("请输入正确的密码");
                    return;
                }

                pop.setText("");
//                Environment.pin = pinInput.getText();
                Environment.alive = true;
                Environment.depteName = sysUser.getDeptName();
                Environment.deptId = sysUser.getDeptId();
                Environment.username = sysUser.getUsername();
                Environment.password = sysUser.getPassword();

                stage.hide();
                try {
                    new Operator(self).show();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                if (parent != null) {
                    parent.show();
                }
            }
        });

    }

    public void show() {
        stage.show();
    }

    public void hide() {
        stage.hide();
    }

    private void pop() {
        //创建另一个stage
        Stage newStage = new Stage();
        newStage.setScene(null);
        //指定 stage 的模式
        newStage.initModality(Modality.APPLICATION_MODAL);
        newStage.setTitle("Pop up window");
    }
}
