package cn.wx.fileclient.ui;

import java.io.FileNotFoundException;

/**
 * Created by IntelliJ IDEA.
 * cn.wx.fileclient.ui
 * name：FileClientWindow
 * user：pengpeng
 * date:2020/11/2
 * time:11:52
 */
public interface FileClientWindow {
    void start() throws FileNotFoundException;
    void show();
    void hide();
}
