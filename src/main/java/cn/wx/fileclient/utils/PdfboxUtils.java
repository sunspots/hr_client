package cn.wx.fileclient.utils;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripperByArea;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Slf4j
public class PdfboxUtils {
    public static final String REGION_NAME = "content";
    /**
     * 根据指定文件页码的指定区域读取文字
     *
     * @param filePath PDF文件路径
     * @param iPage PDF页码
     * @param textRrect 读取文字的区域
     * @return 文字内容
     */
    public static String readRectangelText(String filePath, int iPage, Rectangle textRrect) {

        String textContent = "";
        try {
            PDDocument pdfDoc = PDDocument.load(new File(filePath));
            try{
                // 获取指定的PDF页
                PDPage pdfPage = (PDPage) pdfDoc.getDocumentCatalog().getAllPages().get(iPage);
                // 获取指定位置的文字（文字剥离器）
                PDFTextStripperByArea textStripper = new PDFTextStripperByArea();
                textStripper.setSortByPosition(true);
                textStripper.addRegion(REGION_NAME, textRrect);
                textStripper.extractRegions(pdfPage);
                textContent = textStripper.getTextForRegion(REGION_NAME);
                // 释放资源
                pdfDoc.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }finally {
                pdfDoc.close();
            }
        }catch (IOException ioException){
            log.error("文字内容识别失败");
        }
        return textContent;

    }


    /**
     * 根据指定文件页码的指定区域读取文字
     * @param srcFile
     * @param iPage
     * @param textRrect
     * @return
     */
    public static String readRectangelText(File srcFile, int iPage, Rectangle textRrect) {
        String textContent = "";
        try {
            PDDocument pdfDoc = PDDocument.load(srcFile);
            try{
                // 获取指定的PDF页
                PDPage pdfPage = (PDPage) pdfDoc.getDocumentCatalog().getAllPages().get(iPage);
                // 获取指定位置的文字（文字剥离器）
                PDFTextStripperByArea textStripper = new PDFTextStripperByArea();
                textStripper.setSortByPosition(true);
                textStripper.addRegion(REGION_NAME, textRrect);
                textStripper.extractRegions(pdfPage);
                textContent = textStripper.getTextForRegion(REGION_NAME);


            } catch (Exception ex) {
                log.error("文件内容读取失败",srcFile.getName());
            }finally {
                // 释放资源
                pdfDoc.close();
            }
        }catch (IOException ioException ){
            log.error("文件装载失败",srcFile.getName());
        }
        return textContent;

    }




    public static int fileSize(String filePath) {

        int pdfPage = 0 ;
        try {
            PDDocument pdfDoc = PDDocument.load(new File(filePath));
            try {
                // 获取指定的PDF页
                pdfPage =  pdfDoc.getDocumentCatalog().getAllPages().size();
                pdfDoc.close();
            } catch (Exception ex) {
                log.error("获取文件页数失败");
            }
        }catch (IOException ioException){
            log.error("文件装载失败",filePath);
        }
        return pdfPage;
    }

    /**
     * 根据指定文件页码的指定区域读取图片
     *
     * @param filePath PDF文件路径
     * @param iPage PDF页码
     * @param imgRrect 读取图片的区域
     * @return 图片内容
     */
    public static BufferedImage readRectangelImage(String filePath, int iPage, Rectangle imgRrect) {

        BufferedImage bufImage = null;
        try(PDDocument pdfDoc = PDDocument.load(new File(filePath))) {
            // 获取渲染器，主要用来后面获取BufferedImage
//            PDFRenderer pdfRenderer = new PDFRenderer(pdfDoc);
//            // 截取指定位置生产图片
//            bufImage = pdfRenderer.renderImage(iPage).getSubimage(imgRrect.x,imgRrect.y,imgRrect.width,imgRrect.height);
            // 释放资源
            pdfDoc.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return bufImage;
    }
}
