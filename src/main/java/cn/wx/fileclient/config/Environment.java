package cn.wx.fileclient.config;

import cn.wx.fileclient.ui.Listener;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by IntelliJ IDEA.
 * cn.wx.fileclient.config
 * name：Environment
 * user：pengpeng
 * date:2020/11/2
 * time:12:00
 */
public class Environment {
    //    public static String spiderPath="C:\\Users\\dell\\Desktop\\t_m_spider\\shell\\main.py";
//    public static String spiderPath="..\\shell\\t_m_spider\\shell\\main.py";
    //用户名 可进行登录时初始化，与服务器交互式时直接使用
    public static String username = "";
    public static String password = "";

    public static String host="localhost";
    public static int port=65000;
    public static boolean alive=true;
    public static String pin;
    public static boolean pinValid=true;
    public static boolean answerRun;
    public static boolean accountRun;
    public static AtomicInteger accountDownCount=new AtomicInteger();
    public static AtomicInteger accountDownTotal=new AtomicInteger();
    public static AtomicInteger answerDownCount=new AtomicInteger();
    public static AtomicInteger answerDownTotal=new AtomicInteger();
    public static boolean recAnswerTotal;
    public static boolean recAccountTotal;
    public static String answerStart;
    public static String answerEnd;
    public static String accountStart;
    public static String accountEnd;

    public   static String depteName ;

    public static Long deptId;

    public static Process spider;

    public static String answerFilePath;
    public static String accountFilePath;

    static{
        Listener listener = new Listener();
        listener.setDaemon(true);
        listener.start();
    }

    public static void init() {
        pinValid=true;
        answerRun=false;
        accountRun=false;
        accountDownCount.set(0);
        accountDownTotal.set(0);
        answerDownCount.set(0);
        answerDownTotal.set(0);
        recAnswerTotal=false;
        recAccountTotal=false;
        answerStart=null;
        answerEnd=null;
        accountStart=null;
        accountEnd=null;
        answerFilePath=null;
        accountFilePath=null;
    }

}
