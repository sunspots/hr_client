package cn.wx.fileclient.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * cn.wx.fileclient.entity
 * name：SysUser
 * user：pengpeng
 * date:2020/11/2
 * time:12:52
 */
@Data
public class SysUser {
    private Long id;
    private String username;
    private String name;
    private String password;
    private String deptName;
    private Long deptId;
    private LocalDateTime creatTime;
}
