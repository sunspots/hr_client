package cn.wx.fileclient;

import cn.wx.fileclient.tesseract.OcrChineseRecognizer;
import cn.wx.fileclient.utils.Word2PDFUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * cn.wx.fileclient
 * name：FileClientApplicationTest
 * user：pengpeng
 * date:2020/11/6
 * time:16:09
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class FileClientApplicationTest {
    @Test
    public  void test(String[] args) {
        String path = "E:\\test\\sss.png";
        System.out.println("ORC Test Begin......");
        try {
            String valCode = new OcrChineseRecognizer().recognizeText(new File(path), "PNG");
            System.out.println(valCode);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("ORC Test End......");
    }

    /*@Test
    public void testWordConvertToPDF(){
        File wordFile = new File("E:\\test\\dm.doc");
        Word2PDFUtil.convertToPDF(wordFile);
    }*/
}
