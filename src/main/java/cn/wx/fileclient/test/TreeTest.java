package cn.wx.fileclient.test;

import sun.reflect.generics.tree.Tree;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * cn.wx.fileclient.test
 * name：TreeTest
 * user：pengpeng
 * date:2020/11/3
 * time:14:40
 */
public class TreeTest {
    private static TreeNode tree;
    private static List<TreeNode> nodeList = new ArrayList<>();

    public static void main(String[] args) {
        nodeList.add(new TreeNode(0, 0, 0));
        nodeList.add(new TreeNode(1, 0, 20));
        nodeList.add(new TreeNode(2, 0, 30));
        nodeList.add(new TreeNode(3, 1, 10));
        nodeList.add(new TreeNode(4, 1, 15));
        nodeList.add(new TreeNode(5, 3, 35));
        nodeList.add(new TreeNode(6, 2, 22));
        nodeList.add(new TreeNode(7, 4, 22));
        nodeList.add(new TreeNode(8, 6, 22));
        nodeList.add(new TreeNode(9, 3, 22));
        tree = nodeList.get(0);
        creatTree(nodeList);
        int[] scores = new int[4];
        for (int j = 0; j < 4; j++)
            scores[j] = 0;
        getMaxScore(scores, tree, 0);
        System.out.println(scores);

    }

    /**
     * 时间复杂度 O(n),空间复杂度(T(ln(n)))
     * @param score
     * @param node
     * @param height
     */
    private static void getMaxScore(int[] score, TreeNode node, Integer height) {
        if (null==node)
            return ;
        //本层分数等于上层加上node.score最大值,第0层就是最大值，进行深度遍历
        if (height==0){
            score[height] = node.getScore()>score[height]?node.getScore():score[height];
        }else {
            score[height] = node.getScore()+score[height-1]>score[height]?
                    node.getScore()+score[height-1]:score[height];
        }
        node.getChild().forEach(nodeChild->{
            getMaxScore(score,nodeChild,height+1);
        });

    }

    /**
     * 时间复杂度 O(n),空间复杂度(T(n))
     * @param nodeList
     */
    private static void creatTree(List<TreeNode> nodeList) {
        //存放所有数据，用于快速查找
        Map<Integer, TreeNode> treeCMap = new HashMap<>();
        nodeList.forEach(node -> { treeCMap.put(node.getId(), node);});
        //构建树
        nodeList.forEach(node -> {
            if (node.getId() != 0)
                treeCMap.get(node.getParentId()).addChild(node);
        });
    }
}
